---
layout: post
tags:
  - Groupes
  - Authentification
title:  "Différence entre groupes et rôles"
date:   2018-03-29 17:53:40 +0200
categories: post
---

Quelle est la différences entre groupes et rôles dans FD ?
==========================================================

Dans FusionDirectory (FD), il y a ... deux notions de groupes et un de rôles
qu'il ne faut pas confondre.


Les groupes unix
----------------

Là il s'agit comme au « bon vieux temps » des groupes unix au sens... unix
du terme. L'objet LDAP correspondant est un *PosixGroup*, c'est à
qu'il y a un attribue numérique *gid* associé. Cette attribue peut être
accéder sous le nom *gidNumber*.

Par exemple

```yaml
dn: cn=trucmuche,ou=posixgroups,ou=labos,dc=obspm,dc=fr
cn: electro
objectClass: posixGroup
objectClass: sambaGroupMapping
sambaSID: S-1-5-21-9258509-1860835513-996465392-1034
sambaGroupType: 2
displayName: trucmuche
memberUid: usera
memberUid: userb
memberUid: userc
memberUid: userd
memberUid: usere
structuralObjectClass: posixGroup
gidNumber: 21395
description: _Groupe Posix pour trucmuche
```
L'utilisation de cette objet bien sur le partage de fichier sur un système
Unix.

Les groupes LDAP
----------------

Là c'est relativement différent, il s'agit d'un groupe au sens LDAP du
terme. L'une de différence avec le groupe unix et qu'il n'y a pas de
*gidNumber*. Il n'est donc pas possible de s'en servir pour un partage de
fichier sur un serveur Unix. C'est normal ce n'est pas fait pour.
Un groupe au sens LDAP du terme est un objet connu sous le nom de *groupOfNames*.
Par exemple

```yaml
dn: cn=test,ou=groups,ou=labosX,dc=obspm,dc=fr
objectClass: groupOfNames
objectClass: gosaGroupOfNames
cn: test
description: test
gosaGroupObjects: [U]
member: uid=user1,ou=people,ou=labos1,dc=obspm,dc=fr
member: uid=user2,ou=people,ou=labos2,dc=obspm,dc=fr
structuralObjectClass: groupOfNames
```

Il s'agit d'un objet très important. Car cette objet est reconnu par
quasiment tous les logiciels, de plus un plugin (activé sur notre LDAP)
permet de connaitre à quelle groupe appartient une personne (il s'agit de
l'overlay *memberOf*). Donc un test d'appartenance à un groupe est
extrêmement rapide dans les deux sens à savoir qu'un groupe contient une
personne ou bien qu'une personne appartient à un groupe. L'overlay
*memberOf* permet de construire à la volée un attribue multivalué associé à
chaque personne. Par exemple si on prend l'exemple ci-dessus:

```yaml
dn: uid=user1,ou=people,ou=labos1,dc=obspm,dc=fr
uid: user1
....
memberOf: cn=test,ou=groups,ou=labosX,dc=obspm,dc=fr
```

et

```yaml
dn: uid=user1,ou=people,ou=labos2,dc=obspm,dc=fr
uid: user1
....
memberOf: cn=test,ou=groups,ou=labosX,dc=obspm,dc=fr
```

Point **important**, un groupe (comme l'exemple donnée ci-dessus) peut
contenir des gens de n'importe où dans les branches.

Il s'agit vraiment de l'objet *principale* dès que vous pensez
*autorisations*. Voir par exemple pour [apache]({{ site.baseurl }}{% link _posts/2018-03-30-Apache.markdown %})

Les rôles dans LDAP
-------------------

Les rôles dans LDAP ne sont pas vraiment utilisé dans notre annuaire hors
de l'annuaire. Pour schématiser, les rôles sont comme les groupes LDAP,
mais ne sont grosso-modo compris que par LDAP. Ils sont donc utilisé si
vous voulez définir des ACL (au sens FD du terme). Imaginons que vous
voulez déléguer la gestion des attribues Supann. Il faudra alors créer un
rôle supann_admin. Une fois cela fais vous pourrez associer
une ACL (au sens FD du terme).
