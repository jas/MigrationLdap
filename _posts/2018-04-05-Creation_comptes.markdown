---
layout: post
tags:
  - FusionDirectory
title:  "Création de comptes dans FD"
date:   2018-04-05 10:32:40 +0200
categories: post
---

Création de compte dans FD.
===========================

Pour la création de compte dans FD il faut aller dans le menu « Utilisateurs » (celui en haut avec un «s»),
celui en bas (sans «s») c'est pour gérer votre *propre* compte.

Une fois cliquer vous allez voir ![FD creation utilisateurs 1]({{ "/assets/fd_user_01.png" | absolute_url }})
il faut alors bien vérifier que vous étés dans la branche dans laquelle
vous voulez créer un compte. Cliquez sur le bouton *Action* et sélectionnez
**exclusivement** l'option *Depuis un modèle*. L'autre action n'est pas
interdit, mais cela vous compliquera la vie.

Ensuite vous pourrez choisir le modèle à partir duquel vous voulez créer un
compte.

Le plus complet des modèles est *01_mail_unix*, vous pouvez toujours
partir de ce modèle et ensuite réduire les accès, ou bien partir sur
d'autres modèle. Le *01* est là juste pour ordonner la liste

Supposons que vous partez sur le modèle *01_mail_unix*, vous aurez
alors quelque chose comme ![FD creation utilisateurs 2]({{ "/assets/fd_user_02.png" | absolute_url }})
dans lequel vous allez entrer le nom et prénom, il s'agit bien des «vrais»
nom et prénom dans le sens que si l'utilisateur veut des accents c'est bien
à ces endroits qu'il faut les mettres. Il faut aussi donner le mot de passe
pour l'utilisateur.

Une fois ces informations cliquer sur le bouton *continuer*, vous tomberez
alors sur la page des informations complètes pour un compte, mais avec les
champs *critique* déjà remplis. Merci de ne **pas** les modifier.

Ne **surtout** pas oublier de cliquer lorsque vous aurez fini sur le bouton
*Ok* en bas à droite sinon le compte ne sera pas effectivement créer.
