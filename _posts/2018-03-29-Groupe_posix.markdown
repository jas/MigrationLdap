---
layout: post
tags:
  - Groupe
title:  "Les groupes primaires posix"
date:   2018-03-29 17:53:40 +0200
categories: post
---

Les groupes primaires posix
===========================

Historiquement un compte unix était associé à un groupe primaire, c'est le *gid* du
compte. Il est cependant très difficile de changer le groupe primaire d'un
utilisateur. Si on le fait sans précaution c'est la cata quasiment assuré.

C'est pour cette raison que dans notre annuaire actuelle on traine de gens
qui appartiennent au groupe primaire X alors que cela fait 10 ans voir plus
qu'ils n'ont plus aucun rapport avec X.

C'est pourquoi au fils du temps le groupe primaire est devenue identique à
l'*uid*, c'est à dire qu'il y a autant de groupe primaire que de *uid*, ou
dit autrement on rend caduc l'utilisation du groupe primaire.

Comment migrer
--------------

Il n'y a pas de solution miracle pour la migration des fichiers, il faut
faire des ensembles de find/grep/chgrp pour que tout fonctionne à nouveau.
