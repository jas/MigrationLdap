---
layout: post
tags:
  - Synchronisation
title:  "Systeme de messages"
date:   2018-04-19 16:53:40 +0200
categories: post
---


Système de messages
===================

Une question récurrente pour le LDAP est de savoir comment mettre à jour
vos propres systèmes, par exemple si vous avez un serveur de fichiers (peut
être que c'est un exemple foireux) samba et que vous devez créer des
comptes, ou faire une opération quelconque à chaque rajout/modification de
compte dans l'annuaire LDAP.


RabbitMQ
========

On a mis en place un système de messages AMQP (Advanced Message Queuing
Protocol) à chaque modification dans l'annuaire LDAP.

L'idée est la suivante, chaque fois qu'il y a une modification dans notre
annuaire, un système envoie un message (vous pouvez le voir comme un mail
si vous voulez) à un système de message (rabbitmq) sur notre serveur
(bunny.obspm.fr).

Vous pouvez ensuite recevoir ces messages, à la vitesse que vous voulez et
quand vous voulez, soit via un système de crontab, soit via un système qui
écoute en permanence (auquel cas les modifications qui seront chez vous
seront quasiment synchrone).

*Attention* un message reçu est détruit. Donc si vous avez des actions
divers à faire il faut créer plusieurs queue (voir plus loin)


Recevoir les messages
---------------------

Pour recevoir les messages il faut créer une queue qui va « s'abonner » au
annonces. Il y a des queues qui sont déjà créer pour chaque labos qui sont
connus sous le nom

    labos.obspm.fr

Mais vous pouvez parfaitement créer vos propres queues et abonnez la queue
aux annonces.

Lors de la création de la queue selon son utilisations vous pouvez la
rendre persistante (elle restera après que vous vous étés déconnecter) ou
non. Un point important est de savoir si vous voulez que les messages recu
lorsque vous n'écoutez pas soit conservé. Dans le cas positif il faut créer
la queue persistante, sans l'option auto-delete, et avec un x-message-ttl
aussi long que vous voulez (attention l'unité est en milliseconde).

Techniquement
-------------

Voici un exemple en ruby

```ruby

#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bunny'
require 'json'

connection = Bunny.new(hostname: 'bunny.obspm.fr', vhost: '***', user: '***', pass: '***')
connection.start

channel = connection.create_channel
reception = channel.fanout('annonces', opt = { durable: true })

dio = channel.queue('dio.obspm.fr', auto_delete: false, durable: true, arguments: { 'x-message-ttl' => 120_000 }).bind(reception)

begin
  puts ' [*] Waiting for messages. To exit press CTRL+C'
  dio.subscribe(block: true) do |_delivery_info, _properties, body|
    lejson = JSON.parse(body)
    ap lejson
  end
rescue Interrupt => _
  connection.close
  exit(0)
end
```

Donc si on regarde ligne par ligne :

On commence par mettre en place une connection vers le serveur rabbitmq,
regarder le vhost exactement comme un vhost apache.

```ruby
connection = Bunny.new(hostname: 'bunny.obspm.fr', vhost: '***', user: '***', pass: '***')
connection.start
```

Une fois la connection établie, on se connecte sur un canal, (en ruby c'est
identique à une création).

```ruby
channel = connection.create_channel
reception = channel.fanout('annonces', opt = { durable: true })
```

On créer une queue qu'on attache au canal, tout comme pour la création d'un
canal, en ruby c'est la même commande pour la création d'un canal ou pour
se connecter à un canal qui existe déjà.

```ruby
dio = channel.queue('dio.obspm.fr', auto_delete: false, durable: true, arguments: { 'x-message-ttl' => 120_000 }).bind(reception)
```

Ensuite on écoute sur le canal et on attend

```ruby
begin
  puts ' [*] Waiting for messages. To exit press CTRL+C'
  dio.subscribe(block: true) do |_delivery_info, _properties, body|
    lejson = JSON.parse(body)
    ap lejson
  end
rescue Interrupt => _
  connection.close
  exit(0)
end
```

Format
------

Le format des messages que nous envoyons est une string encoder pour
pouvoir être transformer en un json

```json
{
  "dn" => "LE_DN_CONCERNE",
  "modify_action" => "add/delete/replace",
  "modify_attribute" => "l'attribue qui a ete modifie dans le dn"
}
```

Vous pouvez juste utilser le dn si vous voulez faire des scripts simple.
Le modify_action indique l'opération effectuer dans le dn (changement de
nom par exemple).

Une autre information est envoyé dans la routing_key c'est s'il s'agit d'un
ADD/MODIFY/REPLACE (de dn), par exemple le rajout d'une personne sera un
ADD, etc.

Cette information se trouve dans le premier champs passer dans le bloc (ici:_delivery_info)

Il y a de nombreux langages qui possèdent des librairies permettant de
causer avec rabbitmq. Vous trouverez par exemple sur le site de [rabbitmq](https://www.rabbitmq.com/tutorials/tutorial-three-python.html)
Voici quelques exemples en ruby.

Exemples
--------

Donc par exemple très simple du lancement d'un script shell qui prend en
argument le dn et qui fait ce que vous voulez.

```ruby
begin
  puts ' [*] Waiting for messages. To exit press CTRL+C'
  dio.subscribe(block: true) do |_delivery_info, _properties, body|
    lejson = JSON.parse(body)
    dn = lejson[:dn]
    system ( "/usr/local/bin/mon_super_script_shell #{dn}")
  end
rescue Interrupt => _
  connection.close
  exit(0)
end
```

Vous pouvez aussi utilisez les informations de la routing_key

```ruby
begin
  puts ' [*] Waiting for messages. To exit press CTRL+C'
  dio.subscribe(block: true) do |delivery_info, _properties, body|
    type = delivery_info[:routing_key]
    lejson = JSON.parse(body)
    dn = lejson[:dn]
    if type == 'MODIFY'
      system ( "/usr/local/bin/mon_super_script_shell_qui_modify #{dn}")
    elsif type == 'DELETE'
      system ( "/usr/local/bin/mon_super_script_shell_qui_fait_delete #{dn}")
    elsif type == 'ADD'
      system ( "/usr/local/bin/mon_super_script_shell_qui_fait_add #{dn}")
    else
      puts 'La DIO envoie des messages avec une mauvaise routing key'
    end
  end
rescue Interrupt => _
  connection.close
  exit(0)
end
```
