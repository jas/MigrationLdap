---
layout: post
title:  "Introduction site web"
date:   2018-03-29 15:59:40 +0200
categories: post
---

Sur ce site web vous allez trouver toutes les informations necessaire pour
la migration du LDAP.

Le but de ce site est d'avoir une existence la **PLUS COURTE** possible.

Dès que la migration sera faite ce site disparait.
