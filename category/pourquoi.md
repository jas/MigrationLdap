---
layout: category
title: Pourquoi
---

Pourquoi on chercher à migrer le LDAP ?

Il y a de nombreuse raison de cette migrations. En voici un petit aperçu

    1. Notre annuaire LDAP actuelle est vieillisant, et il faut passer à la
       nouvelle configuration (la configuration de LDAP est à l'intérieur
       de LDAP).

    2. L'interface web d'administration est incompréhensible. Et elle n'est
       pas délégable.

    3. Notre annuaire LDAP impose que les modifications soient faites par
       un compte netadmin partagé. Ce qui veut dire qu'un netadmin qui part
       de l'Observatoire, part aussi avec le mot de passe d'administration
       de sa branche. Sauf à changer ce mot de passe régulièrement.

    4. On détermine actuellement dans notre annuaire l'apparatenance d'une
       personne à un groupe via le nom de sa branche. C'est très
       contraignant (une personne ne peux être que dans une seule branche).

